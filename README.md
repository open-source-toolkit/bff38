# Windows Server 2012 R2 原版SxS资源说明

在部署Windows Server 2012 R2服务器过程中，常遇到的一个挑战是安装.NET Framework 3.5时失败的问题。由于某些环境或政策限制，通过在线Windows Update服务可能无法顺利完成此操作。为此，本仓库特别提供了Windows Server 2012 R2原版的SxS（Side-by-Side）文件夹，旨在帮助用户离线解决.NET Framework 3.5的安装问题。

## 资源用途

- **适用场景**：当您尝试通过Server Manager或DISM命令安装.NET Framework 3.5失败时，本资源尤为关键。
- **解决方案**：无需互联网连接，您可以直接利用这个原始SxS文件夹来手动完成框架的安装配置。

## 使用方法

1. **下载资源**：首先从本仓库下载提供的“wiindows server 2012 r2 原版sxs”压缩包，并解压到您的计算机上，推荐解压至C盘根目录，创建一个名为`C:\sxs`的文件夹并放置其中。
   
2. **执行安装**：
   - 打开提升权限的命令提示符（以管理员身份运行）。
   - 输入以下命令以指向您的备用SxS路径并执行安装：
     ```
     dism /online /enable-feature /featurename:NetFX3 /all /source:C:\sxs /limitaccess
     ```
   - 上述命令会告诉系统从C:\sxs路径中安装.NET Framework 3.5功能，而不需要访问Microsoft服务器。

## 注意事项

- 确保您的操作系统是Windows Server 2012 R2，并且有足够权限执行上述操作。
- 在执行任何系统级别的更改前，建议做好系统备份，以防不测。
- 如果在使用过程中遇到任何问题，可以查找相关技术论坛或社区寻求帮助。

## 结语

借助本仓库提供的资源，您可以有效地绕过安装.NET Framework 3.5时可能遇到的网络限制，确保服务器软件环境搭建的顺利进行。希望这份资源能成为您部署过程中的一把利器。如果有其他宝贵反馈或经验分享，欢迎贡献！